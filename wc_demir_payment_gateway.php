<?php
/**
 * Plugin Name: Woocommerce demir bank payment gateway
 * Description: Плагин позволяет оплачивать продукты с помощью Visa Mastercard через Дебмир банк
 * Author: Абсатар уулу Мыктыбек
 * Version: 1.0.0
 * Text Domain: wc_demir_payment_gateway
 * @package   wc-demir-payment-gateway
 * @author    Абсатар уулу Мыктбыек
 */
 
defined( 'ABSPATH' ) or exit;


// Make sure WooCommerce is active
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}

/**
 * Add the gateway to WC Available Gateways
 * 
 * @since 1.0.0
 * @param array $gateways all available WC gateways
 * @return array $gateways all WC gateways + demir bank gateway
 */
function wc_demir_payment_gateway( $gateways ) {
	$gateways[] = 'WC_Demir_Gateway';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_demir_payment_gateway' );

/**
 * Adds plugin page links
 * 
 * @since 1.0.0
 * @param array $links all plugin links
 * @return array $links all plugin links + our custom links (i.e., "Settings")
 */
function wc_demir_payment_gateway_plugin_links( $links ) {

	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=demir_payment_gateway' ) . '">' . __( 'Configure', 'wc-demir-payment-gateway' ) . '</a>'
	);

	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_demir_payment_gateway_plugin_links' );

/**
 * Demir Payment Gateway
 *
 * @class 		WC_Demir_Gateway
 * @extends		WC_Payment_Gateway
 * @version		1.0.0
 * @package		WooCommerce/Classes/Payment
 * @author 		Абсатар уулу Мыктыбек
 */
add_action( 'plugins_loaded', 'wc_demir_payment_gateway_init', 11 );

function wc_demir_payment_gateway_init() {

	class WC_Demir_Gateway extends WC_Payment_Gateway {

		public $clientId;

		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
	  
			$this->id                 = 'demir_payment_gateway';
			$this->icon               = apply_filters('woocommerce_demir_icon', '');
			$this->has_fields         = false;
			$this->method_title       = __( 'Demir', 'wc-demir-payment-gateway' );
			$this->method_description = __( 'Позволяет оплачивать товары через демир банк используя карты Visa и Mastercard', 'wc-demir-payment-gateway' );
		  
			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
		  
			// Define user set variables
			$this->title        = $this->get_option( 'title' );
			$this->clientId     = $this->get_option( 'clientId' );
			$this->description  = $this->get_option( 'description' );
			$this->instructions = $this->get_option( 'instructions', $this->description );
		  
			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page' ) );
		  	add_action(	'woocommerce_receipt_'.$this->id, array(&$this, 'receipt_page'));
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}
	
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		public function init_form_fields() {
	  
			$this->form_fields = apply_filters( 'wc_demir_form_fields', array(
		  
				'enabled' => array(
					'title'   => __( 'Enable/Disable', 'wc-demir-payment-gateway' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable Demir Payment', 'wc-demir-payment-gateway' ),
					'default' => 'yes'
				),
				'clientId' => array(
                    'title' => __('Client ID', 'demir-woocommerce-payment-gateway'),
                    'type' => 'text',
                    'desc_tip' => true
                ),	
				'title' => array(
					'title'       => __( 'Title', 'wc-demir-payment-gateway' ),
					'type'        => 'text',
					'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc-demir-payment-gateway' ),
					'default'     => __( 'Demir Payment', 'wc-demir-payment-gateway' ),
					'desc_tip'    => true,
				),
				'showlogo' => array(
                    'title' => __('Show MasterCard & Visa logos', 'demir-woocommerce-payment-gateway'),
                    'type' => 'checkbox',
                    'label' => __('Show the MasterCard & Visa logo in the payment method section for the user', 'demir-woocommerce-payment-gateway'),
                    'default' => 'yes',
                    'description' => __('Tick to show "fondy" logo', 'demir-woocommerce-payment-gateway'),
                    'desc_tip' => true
                ),
				'description' => array(
					'title'       => __( 'Description', 'wc-demir-payment-gateway' ),
					'type'        => 'textarea',
					'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc-demir-payment-gateway' ),
					'default'     => __( 'Оплата через платежные карты Visa/MasterCard', 'wc-demir-payment-gateway' ),
					'desc_tip'    => true, 
				)
			) );
		}

		/**
         * Demir Logo
         * @return string
         */
        public function get_icon()
        {
            $icon =
                '<img 
                    style="width: 100%;max-width:75px;min-width: 70px;float: right;" 
                    src="' . WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/img/logo.jpg' . '" 
                    alt="Demir Bank Logo" />';
            if ($this->get_option('showlogo') == "yes") {
                return apply_filters('woocommerce_gateway_icon', $icon, $this->id);
            } else {
                return false;
            }
        }

		/**
		 * Output for the order received page.
		 */
		public function thankyou_page($order_id) {
			if ( $this->instructions ) {
				echo wpautop( wptexturize( $this->instructions ) );
			}
			$order = new WC_Order($order_id);
			$order->update_status( 'on-hold', __( 'Awaiting demir payment', 'wc-gateway-demir' ) );			
			$order->reduce_order_stock();
		}

		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			if ( $this->instructions && ! $sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) ) {
				echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			}
		}

		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {
			$order = new WC_Order($order_id);
            $checkout_payment_url = $order->get_checkout_payment_url(true);
            return array(
                'result' => 'success',
                'redirect' => add_query_arg('order_pay', $order_id, $checkout_payment_url)
            );
		}

		/**
         * Order page
         * @param $order
         */
        public function receipt_page($order)
        {
            $this->generate_form($order);
        }

        /**
         * Generate checkout
         * @param $order_id
         * @return string
         */
        function generate_form($order_id)
        {
            $order = new WC_Order($order_id);
            $args = array(
            	'clientid' => $this->clientId,
            	'amount' => round($order->get_total()),
            	'islemtipi' => 'Auth',
            	'taksit' => '',
            	'oid' => '',
            	'okUrl' => $this->get_return_url( $order ),
            	'failUrl' => WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)).'/ErrorPage.php',
            	'rnd' => microtime(),
            	'storekey' => 'TEST1234',
            	'storetype' => '3d_Pay_Hosting',
            	'lang' => 'ru',
            	'currency' => '417',
            	'refreshtime' => '0'
            );
            WC()->cart->empty_cart();
            $out = $this->demir_post($args);
            return $out;
        }

        /**
         * Post data
         * @param array
         */
        function demir_post($args){
        	$clientId = $args['clientid'];
			$amount = $args['amount'];
			$oid = $args['oid'];
			$okUrl = $args['okUrl'];
			$failUrl = $args['failUrl'];
			$rnd = $args['rnd'];
			$currencyVal = $args['currency'];
			$storekey = $args['storekey'];
			$storetype = $args['storetype'];
			$lang = $args['lang'];
			$instalment = "";
			$transactionType = $args['islemtipi'];
			$hashstr = $clientId . $oid . $amount . $okUrl . $failUrl .$transactionType. $instalment .$rnd .$storekey;
			$hash = base64_encode(pack('H*',sha1($hashstr)));
			echo '<center><img style="width: 250px;" src="' . WP_PLUGIN_URL . "/" . plugin_basename(dirname(__FILE__)) . '/img/loading.gif" /></center>';
        	?>
	        <form method="post" action="https://entegrasyon.asseco-see.com.tr/fim/est3Dgate">
			    <input type="hidden" name="clientid" value="<?php echo $clientId; ?>" />
		        <input type="hidden" name="amount" value="<?php echo $amount; ?>" />
		        <input type="hidden" name="islemtipi" value="<?php echo $transactionType; ?>" />
		        <input type="hidden" name="taksit" value="<?php echo $instalment; ?>" />
		        <input type="hidden" name="oid" value="<?php echo $oid; ?>" />
		        <input type="hidden" name="okUrl" value="<?php echo $okUrl; ?>" />
		        <input type="hidden" name="failUrl" value="<?php echo $failUrl; ?>" />
		        <input type="hidden" name="rnd" value="<?php echo $rnd; ?>" />
		        <input type="hidden" name="hash" value="<?php echo $hash; ?>" />
		        <input type="hidden" name="storetype" value="<?php echo $storetype; ?>" />
		        <input type="hidden" name="lang" value="<?php echo $lang; ?>" />
		        <input type="hidden" name="currency" value="<?php echo $currencyVal; ?>" />
		        <input type="hidden" name="refreshtime" value="0" />
				<input type="submit" id="pay" style="display: none;">
	    	</form>
	    	<script>
	    		document.getElementById("pay").click();
	    	</script>
        	<?php
        }
  	}
}